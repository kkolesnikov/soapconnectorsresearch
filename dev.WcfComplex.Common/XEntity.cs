﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace dev.WcfComplex.Common
{
    [DataContract]
    public class XEntity : XComplexObject
    {
        [DataMember]
        public Dictionary<string, XComplexObject> Values { get; set; }
    }
}