﻿using System.Runtime.Serialization;

namespace dev.WcfComplex.Common
{
    [KnownType(typeof(XEntity))]
    [KnownType(typeof(XSimpleType))]
    [DataContract]
    public class XComplexObject
    {
    }
}