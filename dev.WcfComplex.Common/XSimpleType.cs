﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace dev.WcfComplex.Common
{
    [DataContract]
    public class XSimpleType : XComplexObject
    {
        [DataMember]
        public object Value { get; set; }
    }
}
