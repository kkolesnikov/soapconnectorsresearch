﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using dev.WcfComplex.Common;

namespace dev.ContractOperations.Wcf.Contracts
{
    [DataContract]
    public class InsuranceInfo : XEntity
    {
        [DataMember]
        public string ProgramId { get; set; }
        [DataMember]
        public string ProgramType { get; set; }
        [DataMember]
        public string ProgramName { get; set; }
        [DataMember]
        public Money Amount { get; set; }
        [DataMember]
        public Money RequiredAmount { get; set; }
        [DataMember]
        public Money LimitAmount { get; set; }
        [DataMember]
        public DateTime? InsuranceStartDate { get; set; }
        [DataMember]
        public Money FeeAmount { get; set; }
        [DataMember]
        public string ProgramUrl { get; set; }
        [DataMember]
        public string ContactPhone { get; set; }
        [DataMember]
        public Money Coverage { get; set; }
        [DataMember]
        public ICollection<string> InsuredAccidents { get; set; }
    }
}