﻿using System;
using System.Runtime.Serialization;
using dev.ContractOperations.Wcf.Contracts;
using dev.WcfComplex.Common;

namespace dev.ContractOperations.Wcf.Cards
{
    [DataContract]
    public class Card : XEntity
    {
        /// <summary>
        ///     Gets or sets the card type.
        /// </summary>
        [DataMember]
        public string CardType { get; set; }


        /// <summary>
        ///     Gets or sets the payment system.
        /// </summary>
        [DataMember]
        public string PaymentSystem { get; set; }

        /// <summary>
        ///     Gets or sets the currency.
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        ///     Gets or sets the debt.
        /// </summary>
        [DataMember]
        public Money DebtAmountMoney { get; set; }

        /// <summary>
        ///     Gets or sets the hold amount.
        /// </summary>
        [DataMember]
        public Money HoldAmountMoney { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether is primary.
        /// </summary>
        [DataMember]
        public bool IsPrimary { get; set; }

        /// <summary>
        ///     Gets or sets the contract id.
        /// </summary>
        [DataMember]
        public string ContractId { get; set; }


        /// <summary>
        ///     Gets or sets the opened date.
        /// </summary>
        [DataMember]
        public DateTime ExpiryDate { get; set; }

        /// <summary>
        ///     Gets or sets the name.
        /// </summary>
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        ///     Gets or sets the name.
        /// </summary>
        public string DisplayName { get; set; }

        [DataMember]
        public bool Hidden { get; set; }

        /// <summary>
        ///     Gets or sets the number.
        /// </summary>
        [DataMember]
        public string Number { get; set; }

        /// <summary>
        ///     Gets or sets the balance.
        /// </summary>
        [DataMember]
        public Money TotalBalanceMoney { get; set; }


        /// <summary>
        ///     Gets or sets the balance.
        /// </summary>
        public decimal? BonusPoints { get; set; }


        /// <summary>
        ///     Gets or sets the state.
        /// </summary>
        [DataMember]
        public string State { get; set; }

        [DataMember]
        public Money AvailableLimitAmountMoney { get; set; }

        /// <summary>
        ///     Gets or sets the deposit amount.
        /// </summary>
        [DataMember]
        public Money DepositAmountMoney { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether debit operations allowed.
        /// </summary>
        [DataMember]
        public bool DebitOperationsAllowed { get; set; }


        /// <summary>
        ///     Gets or sets the image name.
        /// </summary>
        [DataMember]
        public string ImageName { get; set; }

        /// <summary>
        ///     Gets or sets the BackgroundImageName
        /// </summary>
        [DataMember]
        public string BackgroundImageName { get; set; }

        /// <summary>
        ///     Gets or sets the background color.
        /// </summary>
        [DataMember]
        public string BackgroundColor { get; set; }

        [DataMember]
        public Contract Contract { get; set; }

        [DataMember]
        public Limit[] Limits { get; set; }
    }
}