﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using dev.WcfComplex.Common;

namespace dev.ContractOperations.Wcf.Contracts
{
    [DataContract]
    public class TopUpMethod : XEntity
    {
        [DataMember]
        public string ContractId { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public ICollection<Instruction> Instructions { get; set; }

        [DataMember]
        public Contract Contract { get; set; }
    }
}