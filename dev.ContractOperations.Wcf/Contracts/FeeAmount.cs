﻿using System.Runtime.Serialization;
using dev.WcfComplex.Common;

namespace dev.ContractOperations.Wcf.Contracts
{
    [DataContract]
    public class FeeAmount : XEntity
    {
        [DataMember]
        public Money Amount { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Type { get; set; }
    }
}