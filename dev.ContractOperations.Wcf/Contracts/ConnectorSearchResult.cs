﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using dev.WcfComplex.Common;

namespace dev.ContractOperations.Wcf.Contracts
{
    [DataContract]
    public class ConnectorSearchResult : XEntity
    {
        [DataMember]
        public IEnumerable<Contract> Contracts { get; set; }
    }
}