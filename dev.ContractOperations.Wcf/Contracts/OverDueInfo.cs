﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using dev.WcfComplex.Common;

namespace dev.ContractOperations.Wcf.Contracts
{
    [DataContract]
    public class OverdueInfo : XEntity
    {
        [DataMember]
        public int OverduePaymentsCount { get; set; }

        [DataMember]
        public Money OverduePaymentsAmount { get; set; }

        [DataMember]
        public Money OverdueAmount { get; set; }

        [DataMember]
        public Money DebtOverdueAmount { get; set; }

        [DataMember]
        public Money InterestOverdueAmount { get; set; }

        [DataMember]
        public ICollection<FeeAmount> FeesOverdueAmounts { get; set; }
    }
}