﻿using System;
using System.Runtime.Serialization;
using dev.WcfComplex.Common;

namespace dev.ContractOperations.Wcf.Contracts
{
    [DataContract]
    public class Account : XEntity
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the closed at.
        /// </summary>
        [DataMember]public DateTime ClosedDate { get; set; }

        /// <summary>
        ///     Gets or sets the currency code.
        /// </summary>
        [DataMember]
        public string Currency { get; set; }

        /// <summary>
        ///     Gets or sets the interest rate.
        /// </summary>
        [DataMember]
        public decimal InterestRate { get; set; }

        /// <summary>
        ///     Gets or sets the name.
        /// </summary>
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        ///     Gets or sets the number.
        /// </summary>
        [DataMember]
        public string Number { get; set; }

        /// <summary>
        ///     Gets or sets the opened date.
        /// </summary>
        [DataMember]
        public DateTime OpenedDate { get; set; }

        /// <summary>
        ///     Gets or sets the pending in balance.
        /// </summary>
        [DataMember]
        public long PendingInBalance { get; set; }

        /// <summary>
        ///     Gets or sets the payment amount.
        /// </summary>
        [DataMember]
        public long PendingOutBalance { get; set; }

        /// <summary>
        ///     Gets or sets the contract id.
        /// </summary>
        [DataMember]
        public string ContractId { get; set; }

        /// <summary>
        ///     Gets or sets the state.
        /// </summary>
        [DataMember]
        public string State { get; set; }

        /// <summary>
        ///     Gets or sets the balance.
        /// </summary>
        [DataMember]
        public long TotalBalance { get; set; }

        /// <summary>
        ///     Gets or sets the tariff id.
        /// </summary>
        [DataMember]
        public string TariffId { get; set; }

        [DataMember]
        public Contracts.Contract Contract { get; set; }

        #endregion
    }
}