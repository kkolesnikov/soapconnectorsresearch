﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using dev.WcfComplex.Common;

namespace dev.ContractOperations.Wcf.Contracts
{
    [DataContract]
    public class Instruction : XEntity
    {
        [DataMember]
        public string Guidance { get; set; }

        [DataMember]
        public string Notes { get; set; }

        [DataMember]
        public ICollection<Property> Details { get; set; }
    }
}