﻿using System;
using System.Runtime.Serialization;
using dev.WcfComplex.Common;

namespace dev.ContractOperations.Wcf.Cards
{
    [DataContract]
    public class CardHolderData : XEntity
    {
        /// <summary>
        ///     Pan
        /// </summary>
        [DataMember]
        public string Pan
        { get; set; }

        /// <summary>
        ///     ExpireDate
        /// </summary>
        [DataMember]
        public DateTime ExpireDate
        { get; set; }

        /// <summary>
        ///     CardVerificationValue
        /// </summary>
        [DataMember]
        public string CardVerificationValue
        { get; set; }

        /// <summary>
        ///     HolderName
        /// </summary>
        [DataMember]
        public string HolderName
        { get; set; }

        /// <summary>
        ///     PaymentSystem
        /// </summary>
        [DataMember]
        public string PaymentSystem { get; set; }

        /// <summary>
        ///     DesignId
        /// </summary>
        [DataMember]
        public string DesignId
        { get; set; }
    }
}