﻿using System;
using System.Runtime.Serialization;
using dev.WcfComplex.Common;

namespace dev.ContractOperations.Wcf.Contracts
{
    [DataContract]
    public class PaymentsSchedule : XEntity
    {
        [DataMember]
        public string ContractId { get; set; }

        [DataMember]
        public DateTime StartDate { get; set; }

        [DataMember]
        public DateTime EndDate { get; set; }

        [DataMember]
        public Contract Contract { get; set; }
    }
}