﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using dev.ContractOperations.Wcf.Cards;
using dev.WcfComplex.Common;

namespace dev.ContractOperations.Wcf.Contracts
{
    [DataContract]
    public class Contract : XEntity
    {
        [DataMember]
        public string ContractName { get; set; }

        [DataMember]
        public string DisplayName { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public bool Hidden { get; set; }

        [DataMember]
        public string State { get; set; }

        [DataMember]
        public string Type { get; set; }

        [DataMember]
        public string ProductId { get; set; }

        [DataMember]
        public string ProductUrl { get; set; }

        [DataMember]
        public DateTime OpenedDate { get; set; }

        [DataMember]
        public DateTime SigningDate { get; set; }

        [DataMember]
        public DateTime? ClosedDate { get; set; }

        [DataMember]
        public DateTime? ClosingDate { get; set; }

        [DataMember]
        public DateTime? PaymentDueDate { get; set; }

        [DataMember]
        public Money PaymentDueAmount { get; set; }

        [DataMember]
        public Money PaymentDueAmountRest { get; set; }

        [DataMember]
        public Money IncomingDueAmount { get; set; }

        [DataMember]
        public Money AvailableLimitAmount { get; set; }

        [DataMember]
        public Money DepositAmount { get; set; }

        [DataMember]
        public Money CreditAmount { get; set; }

        [DataMember]
        public bool HasOverdue { get; set; }

        [DataMember]
        public string ImageName { get; set; }

        [DataMember]
        public string BackgroundImageName { get; set; }

        [DataMember]
        public string BackgroundColor { get; set; }

        [DataMember]
        public Money LimitAmount { get; set; }

        public Money PayedAmount { get; set; }

        public Money DebtDueAmount { get; set; }

        public Money InterestDueAmount { get; set; }

        public DateTime? GraceDate { get; set; }

        public Money GraceAmount { get; set; }

        public Money GraceAmountRest { get; set; }

        public Money DebtAmount { get; set; }

        public bool LimitClosed { get; set; }

        public DateTime LimitClosingDate { get; set; }

        public decimal InterestRate { get; set; }

        public decimal OriginalInterestRate { get; set; }

        public DateTime StatementDate { get; set; }

        /// <summary>
        ///     Gets or sets the fees due amounts.
        /// </summary>
        public ICollection<FeeAmount> FeesDueAmounts { get; set; }

        /// <summary>
        ///     Gets or sets the overdue info.
        /// </summary>
        public OverdueInfo OverdueInfo { get; set; }

        [DataMember]
        public ICollection<Service> Services { get; set; }
        [DataMember]
        public ICollection<Card> Cards { get; set; }
        [DataMember]
        public ICollection<Account> Accounts { get; set; }
        [DataMember]
        public ICollection<TopUpMethod> TopUpMethods { get; set; }
        [DataMember]
        public ICollection<Limit> ContractLimits { get; set; }
        [DataMember]
        public ICollection<Limit> CardLimits { get; set; }
        [DataMember]
        public PaymentsSchedule PaymentsSchedule { get; set; }
    }
}