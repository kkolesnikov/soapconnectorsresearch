﻿using System;
using System.Runtime.Serialization;
using dev.ContractOperations.Wcf.Cards;
using dev.WcfComplex.Common;

namespace dev.ContractOperations.Wcf.Contracts
{
    [DataContract]
    public class Limit : XEntity
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string InactiveName { get; set; }

        [DataMember]
        public string CurrentName { get; set; }

        [DataMember]
        public string CategoryName { get; set; }

        [DataMember]
        public int SortOrder { get; set; }

        [DataMember]
        public bool? ReadOnly { get; set; }

        [DataMember]
        public string Type { get; set; }

        [DataMember]
        public string Period { get; set; }

        [DataMember]
        public DateTime StartTime { get; set; }

        [DataMember]
        public DateTime EndTime { get; set; }

        [DataMember]
        public bool IsActive { get; set; }

        [DataMember]
        public int? MaxCount { get; set; }

        /// <summary>
        ///     MaxCountRestriction
        /// </summary>
        [DataMember]
        public int? MaxCountRestriction { get; set; }

        [DataMember]
        public int? CurrentCount { get; set; }

        [DataMember]
        public Money MaxAmount { get; set; }

        [DataMember]
        public Money CurrentAmount { get; set; }

        /// <summary>
        ///     MaxAmountRestriction
        /// </summary>
        [DataMember]
        public Money MaxAmountRestriction { get; set; }

        /// <summary>
        ///     CategoryId
        /// </summary>
        [DataMember]
        public string CategoryId { get; set; }

        [DataMember]
        public Card Card { get; set; }

        [DataMember]
        public Contract Contract { get; set; }
    }
}