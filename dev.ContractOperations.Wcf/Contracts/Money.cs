﻿using System.Runtime.Serialization;
using dev.WcfComplex.Common;

namespace dev.ContractOperations.Wcf.Contracts
{
    [DataContract]
    public class Money : XComplexObject
    {
        [DataMember]
        public string CurrencyCode { get; set; }

        [DataMember]
        public long Amount { get; set; }
    }
}