﻿using System;
using System.Runtime.Serialization;
using dev.ContractOperations.Wcf.Cards;
using dev.WcfComplex.Common;

namespace dev.ContractOperations.Wcf.Contracts
{
    [DataContract]
    public class Service : XEntity
    {
        [DataMember]
        public string ContractId
        { get; set; }
        [DataMember]
        public int ServiceId
        { get; set; }
        [DataMember]
        public string ServiceType
        { get; set; }
        [DataMember]
        public string Name
        { get; set; }
        [DataMember]
        public string Description
        { get; set; }
        [DataMember]
        public bool Hidden
        { get; set; }

        [DataMember]
        public bool IsActive
        { get; set; }
        [DataMember]
        public string ChannelType
        { get; set; }

        [DataMember]
        public bool IsAvailable
        { get; set; }
        [DataMember]
        public DateTime AvailabilityDate
        { get; set; }
        [DataMember]
        public DateTime ActivationDate
        { get; set; }
        [DataMember]
        public DateTime DeactivationDate
        { get; set; }
        [DataMember]
        public DateTime NextExecutionDate
        { get; set; }
        [DataMember]
        public InsuranceInfo InsuranceInfo
        { get; set; }
        [DataMember]
        public string Notes
        { get; set; }
        [DataMember]
        public Card Card { get; set; }
    }
}