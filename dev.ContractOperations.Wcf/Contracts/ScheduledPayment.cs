﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using dev.WcfComplex.Common;

namespace dev.ContractOperations.Wcf.Contracts
{
    [DataContract]
    public class ScheduledPayment : XEntity
    {
        [DataMember]
        public DateTime Term
        {
            get; set;
        }
        [DataMember]
        public string State
        {
            get; set;
        }
        [DataMember]
        public Money DebtPaymentsAmount
        {
            get; set;
        }
        [DataMember]
        public Money DueAmount
        {
            get; set;
        }
        [DataMember]
        public Money DebtDueAmount
        {
            get; set;
        }
        [DataMember]
        public Money InterestDueAmount
        {
            get; set;
        }
        [DataMember]
        public ICollection<FeeAmount> FeesDueAmounts
        {
            get; set;
        }
        [DataMember]
        public Money PayedAmount
        {
            get; set;
        }
        [DataMember]
        public Money DebtPayedAmount
        {
            get; set;
        }
        [DataMember]
        public Money InterestPayedAmount
        {
            get; set;
        }
        [DataMember]
        public ICollection<FeeAmount> FeesPayedAmounts
        {
            get; set;
        }
        [DataMember]
        public Money AdvanceAmount
        {
            get; set;
        }
        [DataMember]
        public Money RestAmount
        {
            get; set;
        }

        [DataMember]
        public OverdueInfo OverdueInfo
        {
            get; set;
        }
    }
}