﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using dev.ContractOperations.Wcf.Cards;
using dev.ContractOperations.Wcf.Contracts;

namespace dev.ContractOperations.Wcf
{
    public class ContractOperations : IContractOperations
    {
        public Task<IEnumerable<Contract>> GetContractsAsync(string clientId, bool includeDetails = false)
        {
            throw new NotImplementedException();
        }

        public Task<Contract> GetContractAsync(string contractId, bool includeDetails = false)
        {
            throw new NotImplementedException();
        }

        public Task<ConnectorSearchResult> SearchContractsAsync(ConnectorSearchQuery query, bool includeDetails = false)
        {
            throw new NotImplementedException();
        }

        public Task<PaymentsSchedule> GetPaymentsScheduleAsync(Contract contract)
        {
            throw new NotImplementedException();
        }

        public Task StoreContractPropertiesAsync(Contract contract, IDictionary<string, object> properties)
        {
            throw new NotImplementedException();
        }

        public Task<Card> GetCardAsync(string userId, string cardId, bool includeDetails = false)
        {
            throw new NotImplementedException();
        }

        public Task<Card> GetCardAsync(string userId, Contract contract, string cardId, bool includeDetails = false)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Card>> GetContractCardsAsync(string userId, Contract contract, bool includeDetails = false)
        {
            throw new NotImplementedException();
        }

        public Task<ICollection<Limit>> GetLimitsAsync(string userId, Contract contract, Card card = null)
        {
            throw new NotImplementedException();
        }

        public Task<Limit> GetLimitAsync(string userId, string limitId, Contract contract, Card card = null)
        {
            throw new NotImplementedException();
        }

        public Task UpdateLimitsAsync(string userId, ICollection<Limit> limitsChanges, Contract contract, Card card = null)
        {
            throw new NotImplementedException();
        }

        public Task UpdateLimitAsync(string userId, Limit limitChanges, Contract contract, Card card = null)
        {
            throw new NotImplementedException();
        }

        public Task ResetLimitCountAsync(string userId, string limitId, Contract contract, Card card = null)
        {
            throw new NotImplementedException();
        }

        public Task ResetLimitAmountAsync(string userId, string limitId, Contract contract, Card card = null)
        {
            throw new NotImplementedException();
        }

        public Task<CardHolderData> GetCardHolderDataAsync(string userId, Card card)
        {
            throw new NotImplementedException();
        }

        public Task StoreCardPropertiesAsync(string userId, Contract contract, Card card, IDictionary<string, object> properties)
        {
            throw new NotImplementedException();
        }

        public Task<Account> GetAccountAsync(string userId, Contract contract, string accountId)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Account>> GetContractAccountsAsync(string userId, Contract contract)
        {
            throw new NotImplementedException();
        }

        public Task<TopUpMethod> GetTopUpMethodAsync(string userId, Contract contract, string methodId)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<TopUpMethod>> GetContractTopUpMethodsAsync(string userId, Contract contract, bool includeDetails = true)
        {
            throw new NotImplementedException();
        }

        public Task<Service> GetServiceAsync(string userId, Contract contract, string serviceId)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Service>> GetContractServicesAsync(string userId, Contract contract)
        {
            throw new NotImplementedException();
        }
    }
}