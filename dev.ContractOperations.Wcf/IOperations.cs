﻿using System.Collections.Generic;
using System.ServiceModel;
using System.Threading.Tasks;
using dev.ContractOperations.Wcf.Cards;
using dev.ContractOperations.Wcf.Contracts;

namespace dev.ContractOperations.Wcf
{
    [ServiceContract]
    public interface IContractOperations
    {
        [OperationContract]
        Task<IEnumerable<Contract>> GetContractsAsync(string clientId, bool includeDetails = false);

        [OperationContract]
        Task<Contract> GetContractAsync(string contractId, bool includeDetails = false);

        [OperationContract]
        Task<ConnectorSearchResult> SearchContractsAsync(ConnectorSearchQuery query, bool includeDetails = false);

        [OperationContract]
        Task<PaymentsSchedule> GetPaymentsScheduleAsync(Contract contract);

        [OperationContract]
        Task StoreContractPropertiesAsync(Contract contract, IDictionary<string, object> properties);

        [OperationContract]
        Task<Card> GetCardAsync(string userId, string cardId, bool includeDetails = false);

        [OperationContract]
        Task<Card> GetCardAsync(string userId, Contract contract, string cardId, bool includeDetails = false);

        [OperationContract]
        Task<IEnumerable<Card>> GetContractCardsAsync(string userId, Contract contract, bool includeDetails = false);

        [OperationContract]
        Task<ICollection<Limit>> GetLimitsAsync(
            string userId,
            Contract contract,
            Card card = null);

        [OperationContract]
        Task<Limit> GetLimitAsync(
            string userId,
            string limitId,
            Contract contract,
            Card card = null);

        [OperationContract]
        Task UpdateLimitsAsync(
            string userId,
            ICollection<Limit> limitsChanges,
            Contract contract,
            Card card = null);

        [OperationContract]
        Task UpdateLimitAsync(
            string userId,
            Limit limitChanges,
            Contract contract,
            Card card = null);

        [OperationContract]
        Task ResetLimitCountAsync(
            string userId,
            string limitId,
            Contract contract,
            Card card = null);

        [OperationContract]
        Task ResetLimitAmountAsync(
            string userId,
            string limitId,
            Contract contract,
            Card card = null);

        [OperationContract]
        Task<CardHolderData> GetCardHolderDataAsync(string userId, Card card);

        [OperationContract]
        Task StoreCardPropertiesAsync(string userId, Contract contract, Card card,
            IDictionary<string, object> properties);

        [OperationContract]
        Task<Account> GetAccountAsync(string userId, Contract contract, string accountId);

        [OperationContract]
        Task<IEnumerable<Account>> GetContractAccountsAsync(string userId, Contract contract);

        [OperationContract]
        Task<TopUpMethod> GetTopUpMethodAsync(string userId, Contract contract, string methodId);

        [OperationContract]
        Task<IEnumerable<TopUpMethod>> GetContractTopUpMethodsAsync(string userId, Contract contract,
            bool includeDetails = true);

        [OperationContract]
        Task<Service> GetServiceAsync(string userId, Contract contract, string serviceId);

        [OperationContract]
        Task<IEnumerable<Service>> GetContractServicesAsync(string userId, Contract contract);
    }
}